
const RESULT_VALUES = {
  w: 3,
  d: 1,
  l: 0
}

/**
 * Takes a single result string and (one of 'w', 'l', or 'd') 
 * and returns the point value
 * 
 * @param {string} result 
 * @returns {number} point value
 */
const getPointsFromResult = function getPointsFromResult(result) {
    if (result == 'w') RESULT_VALUES[result] = 3;
	if (result == 'd') RESULT_VALUES[result] = 1;
	if (result == 'l') RESULT_VALUES[result] = 0; 
  return RESULT_VALUES[result];
}

// Create getTotalPoints function which accepts a string of results
// including wins, draws, and losses i.e. 'wwdlw'
// Returns total number of points won
function getTotalPoints(results) {
	let totalNumWins = 0;

 	let resultsArray = results.split('');
	resultsArray.forEach(function(r){
		if (r == 'w') {
			totalNumWins += getPointsFromResult(r);
		}
	});

	return totalNumWins;
}


// Check getTotalPoints
console.log('Total Sounders Wins: ' + getTotalPoints('wwdl')); 
console.log('Total Galaxy Wins: ' + getTotalPoints('wlld'));

// create orderTeams function that accepts as many team objects as desired, 
// each argument is a team object in the format { name, results }
// i.e. {name: 'Sounders', results: 'wwlwdd'}
// Logs each entry to the console as "Team name: points"
function orderTeams(){
	let sum = 0;
	let teamsList = Array.from(arguments);

	teamsList.forEach(function(t) {
		let resultsArray = t.results.split('');

		resultsArray.forEach(function(r){
			sum += getPointsFromResult(r);
		});
		console.log(`${t.name} : ${sum} points total`); 
	});
}


// Check orderTeams
orderTeams(
  { name: 'Sounders', results: 'wwdl' },
  { name: 'Galaxy', results: 'wlld' },
);
// should log the following to the console:
// Sounders: 7
// Galaxy: 4