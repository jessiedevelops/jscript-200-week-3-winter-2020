// Create a function testPhoneNumber 
// takes in a phoneNumber string in one of these formats:
// '(206) 333-4444'
// '206-333-4444'
// '206 333 4444'
// Returns true if valid, false if not valid
function testPhoneNumber(telnum) {
	const pattern = /^(\(*)\d{3}(\)*)[\s-](\d{3})[\s-](\d{4})$/; 
 	
 	return pattern.test(telnum);
}
// Explanation of RegExp
// ^      start of line
// \(     optional start parenthesis
// \d{3}  exactly 3 digit characters
// \)     optional end parenthesis
// [-\s]  one of a space or a dash
// \d{3}  exactly 3 digit characters
// [-\s]  one of a space or a dash
// \d{4}  exactly 4 digit characters
// $      end of line

// check testPhoneNumber
console.log(testPhoneNumber('(206) 333-4444')); // should return true
console.log(testPhoneNumber('(206) 33-4444')); // should return false, missing a digit


// 1. Create a function parsePhoneNumber that takes in a phoneNumber string 
// in one of the above formats.  For this, you can *assume the phone number
// passed in is correct*.  This should use a regular expression
// and run the exec method to capture the area code and remaining part of
// the phone number.
// Returns an object in the format {areaCode, phoneNumber}
function parsePhoneNumber(telnum){
	/* Find Area Code */
	const areaRegex = RegExp(/^(\(*)\d{3}(\)*)/, 'g'); 
	let area;
	let tel_index;
	let array = []; 

	while ((array = areaRegex.exec(telnum)) !== null) {
 		area = array[0]; 
 		tel_index = areaRegex.lastIndex;

 		if (/^[(]/.test(area)){
 			no_pareth_area = area.slice(1, 4);
 			area = no_pareth_area;  			
 		}
 		//console.log('Area code found:  '+ array[0]);
 		//console.log('Next match starts at ' + areaRegex.lastIndex);
	}

	/* Find Telephone Number */
	const telRegex = RegExp(/[\s-](\d{3})[\s-](\d{4})$/, 'g');
	
	let array2 = [];
	let phone;

	while ((array2 = telRegex.exec(telnum)) !== null){
		phone = array2[1] + array2[2];
		//console.log(phone);
	}
	const phoneNumberObj = {
		areaCode: area,
		phoneNumber: phone,
	}
	return phoneNumberObj;
}

// Check parsePhoneNumber
console.log(parsePhoneNumber('206-333-4444'));
// returns {areaCode: '206', phoneNumber: '3334444'}

console.log(parsePhoneNumber('(222) 422-5353'));
// returns {areaCode: '222', phoneNumber: '4225353'}